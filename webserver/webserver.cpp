
#include "blc_program.h"
#include <string.h>
#include <libwebsockets.h>


#define EXAMPLE_RX_BUFFER_BYTES (10)
struct payload
{
    unsigned char data[LWS_SEND_BUFFER_PRE_PADDING + EXAMPLE_RX_BUFFER_BYTES + LWS_SEND_BUFFER_POST_PADDING];
    size_t len;
} received_payload;


struct per_session_data__http {
    lws_fop_fd_t fop_fd;
#ifdef LWS_WITH_CGI
    struct lws_cgi_args args;
#endif
#if defined(LWS_WITH_CGI) || !defined(LWS_NO_CLIENT)
    int reason_bf;
#endif
    unsigned int client_finished:1;
    
    
    struct lws_spa *spa;
    char result[500 + LWS_PRE];
    int result_len;
    
    char filename[256];
    long file_length;
    lws_filefd_type post_fd;
};

static int callback_http(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len) {
    char const *universal_response = "Hello, World toto!\n";
    char *requested_uri = (char *) in;

    int ret=0;
    
    switch (reason) {
        case LWS_CALLBACK_CLIENT_WRITEABLE:
            printf("connection established\n");
                   break;
            
        case LWS_CALLBACK_HTTP:
            printf("requested URI: %s\n", requested_uri);
            lws_serve_http_file(wsi, "test.html", "text/html", NULL, 0);

            lws_callback_on_writable(wsi);
//            lws_write(wsi, (unsigned char*)universal_response, strlen(universal_response), LWS_WRITE_HTTP);
 //           lws_close_reason(wsi, LWS_CLOSE_STATUS_GOINGAWAY, (unsigned char *)"seeya", 5);
            
            break;
        case LWS_CALLBACK_CLOSED_HTTP:
            printf("%s\n", STRINGIFY(LWS_CALLBACK_CLOSED_HTTP));
            break;

        case LWS_CALLBACK_HTTP_WRITEABLE:
            printf("%s\n", STRINGIFY(LWS_CALLBACK_HTTP_WRITEABLE));
          //  lws_write(wsi, (unsigned char*)universal_response, strlen(universal_response), LWS_WRITE_HTTP);

         //   lws_close_reason(wsi, LWS_CLOSE_STATUS_NORMAL, (unsigned char *)"seeya", 5);
            ret=-1; //End of communication
            break;
        case LWS_CALLBACK_CLOSED:
            printf("%s\n", STRINGIFY(LWS_CALLBACK_CLOSED));
            break;
        case LWS_CALLBACK_PROTOCOL_INIT:
            printf("%s\n", STRINGIFY(LWS_CALLBACK_PROTOCOL_INIT));
            break;
        case LWS_CALLBACK_FILTER_HTTP_CONNECTION:
            printf("%s\n", STRINGIFY(LWS_CALLBACK_FILTER_HTTP_CONNECTION));
            break;
        case LWS_CALLBACK_GET_THREAD_ID:
            printf("%s\n", STRINGIFY(LWS_CALLBACK_GET_THREAD_ID));
            break;
        case LWS_CALLBACK_FILTER_NETWORK_CONNECTION:
            printf("%s\n", STRINGIFY(LWS_CALLBACK_FILTER_NETWORK_CONNECTION));
            break;
        case LWS_CALLBACK_SERVER_NEW_CLIENT_INSTANTIATED:
             printf("%s\n", STRINGIFY(LWS_CALLBACK_SERVER_NEW_CLIENT_INSTANTIATED));
            break;
        case LWS_CALLBACK_CHANGE_MODE_POLL_FD:
            printf("%s\n", STRINGIFY(LWS_CALLBACK_CHANGE_MODE_POLL_FD));
            break;
        case LWS_CALLBACK_ADD_POLL_FD:
            printf("%s\n", STRINGIFY(LWS_CALLBACK_ADD_POLL_FD));
            break;
        case LWS_CALLBACK_WSI_CREATE:
            printf("%s\n", STRINGIFY(LWS_CALLBACK_WSI_CREATE));
            break;
        case LWS_CALLBACK_LOCK_POLL:
            printf("%s\n", STRINGIFY(LWS_CALLBACK_LOCK_POLL));
            break;
        case LWS_CALLBACK_UNLOCK_POLL:
            printf("%s\n", STRINGIFY(LWS_CALLBACK_UNLOCK_POLL));
            break;
        case LWS_CALLBACK_WSI_DESTROY:
            printf("%s\n", STRINGIFY(LWS_CALLBACK_WSI_DESTROY));
            break;
        case LWS_CALLBACK_DEL_POLL_FD:
             printf("%s\n", STRINGIFY(LWS_CALLBACK_DEL_POLL_FD));
            break;
        case LWS_CALLBACK_HTTP_BIND_PROTOCOL:
            printf("%s\n", STRINGIFY(LWS_CALLBACK_HTTP_BIND_PROTOCOL));
            break;
        case LWS_CALLBACK_HTTP_DROP_PROTOCOL:
            printf("%s\n", STRINGIFY(LWS_CALLBACK_HTTP_DROP_PROTOCOL));
            break;
            
            
            //	lws_serve_http_file( wsi, "example.html", "text/html", NULL, 0 );
 /*
            if (strcmp(requested_uri, "/") == 0) {
                lws_write(wsi, (unsigned char*)universal_response, strlen(universal_response), LWS_WRITE_HTTP);

                break;
                
            } else {
                // try to get current working directory
                char cwd[1024];
                char *resource_path;
                
                if (getcwd(cwd, sizeof(cwd)) != NULL) {
                    // allocate enough memory for the resource path
                    resource_path = (char*)malloc(strlen(cwd)
                                           + strlen(requested_uri));
                    
                    // join current working directory to the resource path
                    sprintf(resource_path, "%s%s", cwd, requested_uri);
                    printf("resource path: %s\n", resource_path);
                    
                    char *extension = strrchr(resource_path, '.');
                    char *mime;
                    
                    // choose mime type based on the file extension
                    if (extension == NULL) {
                        mime = "text/plain";
                    } else if (strcmp(extension, ".png") == 0) {
                        mime = "image/png";
                    } else if (strcmp(extension, ".jpg") == 0) {
                        mime = "image/jpg";
                    } else if (strcmp(extension, ".gif") == 0) {
                        mime = "image/gif";
                    } else if (strcmp(extension, ".html") == 0) {
                        mime = "text/html";
                    } else if (strcmp(extension, ".css") == 0) {
                        mime = "text/css";
                    } else {
                        mime = "text/plain";
                    }
                    lws_serve_http_file(wsi, resource_path, mime, NULL, 0);
                    
                }
            }*/
        
            // close connection
    //   lws_close_reason(wsi, LWS_CLOSE_STATUS_GOINGAWAY, (unsigned char *)"seeya", 5);
         //   lws_client_http_body_pending(wsi, 0);
            
        default:
            printf("http: unhandled callback '%d'\n", reason);
            break;
    }
    
    return ret;
}
static int callback_example( struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len )
{
    switch( reason )
    {
        case LWS_CALLBACK_RECEIVE:
            memcpy( &received_payload.data[LWS_SEND_BUFFER_PRE_PADDING], in, len );
            received_payload.len = len;
            lws_callback_on_writable_all_protocol( lws_get_context( wsi ), lws_get_protocol( wsi ) );
            break;
            
        case LWS_CALLBACK_SERVER_WRITEABLE:
            lws_write( wsi, &received_payload.data[LWS_SEND_BUFFER_PRE_PADDING], received_payload.len, LWS_WRITE_TEXT );
            break;
            
        default:
            printf("example unhandled callback '%d'\n", reason);

            break;
    }
    
    return 0;
}



static struct lws_protocols protocols[] = {
    /* first protocol must always be HTTP handler */
    {
        "http-only",    /* name */
        callback_http  /* callback */
    },
    {
        NULL, NULL, 0   /* End of list */
    }
    
};


int main(int argc, char **argv) {
    // server url will be http://localhost:9000
    struct lws_context *context;
    struct lws_context_creation_info context_info;

    CLEAR(context_info);
    context_info.port=9000;
    context_info.protocols=protocols;
    
    context = lws_create_context(&context_info);

    
    if (context == NULL) {
        fprintf(stderr, "libwebsocket init failed\n");
        return -1;
    }
    
    printf("starting server...\n");
    
    // infinite loop, to end this server send SIGTERM. (CTRL+C)
    BLC_COMMAND_LOOP(0){
        lws_service(context, 1000);
    }
    
    lws_context_destroy(context);
    
    return 0;
}
