cmake_minimum_required(VERSION 2.6)

project(webserver)


find_package(blc_program REQUIRED)
#find_package(OpenSSL REQUIRED)

set(OPENSSL_INCLUDE_DIR /usr/local/opt/openssl/include)

add_definitions(${BL_DEFINITIONS})
include_directories(${BL_INCLUDE_DIRS} ${OPENSSL_INCLUDE_DIR} /usr/local/include ) #/usr/local/include is for websockets.h
add_executable(webserver webserver.cpp)
target_link_libraries(webserver ${BL_LIBRARIES} ${OPENSSL_LIBRARIES} /usr/local/lib/libwebsockets.dylib )
